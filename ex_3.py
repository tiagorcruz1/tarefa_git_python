# -*- coding: utf-8 -*-
"""
Created on Wed Mar 27 13:53:59 2019

* Quantos são os anagramas da palavra banana?
* 
* -> anagrama é a quantidade de novas palavras formadas com ou sem sentido, 
* utilizando as letras de outra palavra
*
* -> é definido pelo fatorial do número de letras da palavra

@author: Tiago Cruz
"""

import funcoesExternas

palavra = str("banana")

print("Número de anagramas: ", funcoesExternas.fatorial(len(palavra)))




