# -*- coding: utf-8 -*-
"""
Created on Wed Mar 27 10:08:24 2019

* Chama a funcao que calcula o fatorial
* Verifica se o valor informado é um inteiro e se é positivo maior que zero

@author: Tiago Cruz
"""

import funcoesExternas

while True:
   # exception handling block
    try:
        # input() always converts into a string
        # int() convert into a integer type
        num = int(input("Informe o valor: "))
        
        if num <=0:
            raise ValueError # cria uma excecao do tipo valor incorreto
        print(funcoesExternas.fatorial(num))
        break
    
    except ValueError:
        # trata o erro impedindo que o programa quebre
        print("O número deve ser um int>0")