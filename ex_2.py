# -*- coding: utf-8 -*-
"""
Created on Wed Mar 27 14:27:54 2019

* Quantos numeros de quatro digitos podemos formar com os algarismos 3, 4, 5 e 7?

@author: Tiago Cruz
"""

import funcoesExternas

print(funcoesExternas.fatorial(4), " números")