# -*- coding: utf-8 -*-
"""
Created on Thu Mar 14 08:58:21 2019

@author: Tiago Cruz
"""

def fatorial(n):
    """" calcula o fatorial do valor informado """
    
    # range(start, stop, step) default: start = 0; step = 1
    seq = range(n-1, 0, -1)
#    for i in reversed(range(1,n)):
    for i in seq:
        n = n * i
    return n