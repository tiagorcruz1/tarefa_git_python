# -*- coding: utf-8 -*-
"""
Created on Wed Mar 27 14:10:19 2019

* De um grupo de 20 pessoas deseja-se formar uma fila com 5 delas.
* Quantas filas distintas podemos formar? 
*
* Formula geral para calcular o arranjo simples
* A = n!/((n-p)!)   ,onde n= nº total de elementos e p = elementos por arranjo

@author: Tiago Cruz
"""

import funcoesExternas as fe

n = 20
p = 5

A = fe.fatorial(n)/fe.fatorial(n-p)

print("O número de filas distintas é: ", int(A))